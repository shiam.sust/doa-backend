<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doa_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doa_id')->unsigned()->nullable();
            $table->string('fojilot');
            $table->string('doa');
            $table->string('meaning');
            $table->timestamps();

            $table->foreign('doa_id')->references('id')->on('doa_titles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doa_details');
    }
}
