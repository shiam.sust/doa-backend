<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoaDetails extends Model
{
    protected $fillable = ['doa_id', 'doa', 'meaning'];

    public function doaTitle(){
        return $this->belongsTo('App\Model\DoaTitle','doa_id','id');
    }
}
