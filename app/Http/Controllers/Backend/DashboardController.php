<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\DoaTitle;

class DashboardController extends Controller
{
    public function index(){

    	return view('admin.dashboard');
    }

    public function showDoaTitles(){
    	$doas = DoaTitle::orderBy('id','desc')->get();
    	return view('doa.index')->with([
    		'doas' => $doas
    	]);
    }

    public function addDoa(){
    	return view('doa.addDoa');
    }

    public function storeDoa(Request $request){
    	//return $request->all();

    	$this->validate($request, array(
    		'name' => 'required',
    		'sura' => 'required',
    		'ayat_no' => 'required'
    	));

    	$doa = new DoaTitle;
    	$doa->name = $request->name;
    	$doa->sura = $request->sura;
    	$doa->ayat_no = $request->ayat_no;

    	$doa->save();

    	return redirect()->route('show-doa-title')->with([
    		'message' => [
    			'status' => 'alert-success',
    			'text' => 'new doa inserted successfully'
    		]
    	]);
    }

    public function edit($id){
    	$doa = DoaTitle::findOrFail($id);

    	return view('doa.edit')->with([
    		'doa' => $doa
    	]);
    }

    public function update(Request $request, $id){
    	//return $request->all();

    	$this->validate($request, array(
    		'name' => 'required',
    		'sura' => 'required',
    		'ayat_no' => 'required'
    	));

    	$updateDetails = array(
    		'name' => $request->get('name'),
    		'sura' => $request->get('sura'),
    		'ayat_no' => $request->get('ayat_no')
    	);

    	DoaTitle::where('id', $id)->update($updateDetails);

    	return redirect()->route('show-doa-title')->with([
    		'message' => [
    			'status' => 'alert-success',
    			'text' => 'doa-title updated successfully'
    		]
    	]);

    }

    public function delete($id){
    	$doa = DoaTitle::findOrFail($id);
    	$doa->delete();

        return redirect()->route('show-doa-title')->with([
            'message' => [
                'status' => 'alert-success',
                'text' => 'doa-title deleted successfully'
            ]
        ]);
    }

}
