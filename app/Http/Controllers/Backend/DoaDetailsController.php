<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DoaDetails;
use App\Model\DoaTitle;

class DoaDetailsController extends Controller
{
    public function index(){
        $doas = DoaDetails::orderBy('id','desc')->get();
    	return view('doa-details.index')->with([
            'doas' => $doas
        ]);
    }

    public function create(){
        $doas = DoaTitle::orderBy('id','desc')->get();
    	return view('doa-details.addDoaDetails')->with([
            'doas' => $doas
        ]);
    }

    public function store(Request $request) {
    	//return $request->all();
    	$this->validate($request, array(
    		'doa_id' => 'required',
    		'doa' => 'required',
    		'meaning' => 'required'
    	));

    	$doa = new DoaDetails;
    	
    	$doa->doa_id = $request->doa_id;
    	$doa->fojilot = $request->fojilot;
    	$doa->doa = $request->doa;
    	$doa->meaning = $request->meaning;

    	$doa->save();

        //return $doa;

        return redirect()->route('show-doa-details')->with([
            'message' => [
                'status' => 'alert-success',
                'text' => 'new doa details inserted successfully!'
            ]
        ]);

    }

    public function edit($id){
        $doa = DoaDetails::findOrFail($id);
        $doa_title = DoaTitle::all();

        return view('doa-details.edit')->with([
            'doa' => $doa,
            'doa_title' => $doa_title
        ]);
    }

    public function update(Request $request, $id){
        //return $request->all();

        $this->validate($request, array(
            'doa_id' => 'required',
            'doa' => 'required',
            'meaning' => 'required'
        ));
        
        $updateDetails = array(
            'doa_id' => $request->get('doa_id'),
            'doa' => $request->get('doa'),
            'meaning' => $request->get('meaning')
        );

        DoaDetails::where('id', $id)->update($updateDetails);

        return redirect()->route('show-doa-details')->with([
            'message' => [
                'status' => 'alert-success',
                'text' => 'doa-details updated successfully!'
            ]
        ]);

    }

    public function delete($id){
        $doa = DoaDetails::findOrFail($id);
        $doa->delete();

        return redirect()->route('show-doa-details')->with([
            'message' => [
                'status' => 'alert-success',
                'text' => 'doa-details deleted successfully!'
            ]
        ]);
    }

    public function getAllDoa(){
        $doa = DoaDetails::with('DoaTitle')->get();

        return response()->json($doa);
    }   

    
}
