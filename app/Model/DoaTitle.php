<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoaTitle extends Model
{
    protected $fillable = ['name'];

    public function products(){
    	return $this->hasMany('App\DoaDetails','doa_id','id');
    }
}
