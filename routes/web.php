<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::group(['middleware' => 'auth'], function(){

	Route::get('logout', function(){
		Auth::logout();
		return redirect()->route('login');
	});

	Route::get('dash', 'Backend\DashboardController@index');

	Route::get('show-doa-title',[
		'uses' => 'Backend\DashboardController@showDoaTitles'
	])->name('show-doa-title');
	
	Route::get('new-doa',[
		'uses' => 'Backend\DashboardController@addDoa',
		'as' => 'new-doa'
	]);

	Route::post('doa', [
		'uses' => 'Backend\DashboardController@storeDoa',
		'as' => 'post.doa'
	]);

	Route::get('edit-doa/{id}', [
		'uses' => 'Backend\DashboardController@edit',
		'as' => 'edit-doa'
	]);

	Route::get('delete-doa/{id}', [
		'uses' => 'Backend\DashboardController@delete',
		'as' => 'delete-doa'
	]);

	Route::post('update/{id}', [
		'uses' => 'Backend\DashboardController@update',
		'as' => 'update-doa'
	]);


	//Doa Details
	Route::get('show-doa-details',[
		'uses' => 'Backend\DoaDetailsController@index'
	])->name('show-doa-details');

	Route::get('add-doa-details',[
		'uses' => 'Backend\DoaDetailsController@create',
		'as' => 'add-doa-details'
	]);

	Route::post('doa-details', [
		'uses' => 'Backend\DoaDetailsController@store',
		'as' => 'post.doa-details'
	]); 

	Route::get('edit-doa-details/{id}', [
		'uses' => 'Backend\DoaDetailsController@edit',
		'as' => 'edit-doa-details'
	]);

	Route::post('update-doa-details/{id}', [
		'uses' => 'Backend\DoaDetailsController@update',
		'as' => 'update-doa-details'
	]);

	Route::get('delete-doa-details/{id}', [
		'uses' => 'Backend\DoaDetailsController@delete',
		'as' => 'delete-doa-details'
	]);


});
