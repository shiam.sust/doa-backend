@extends('admin.dashboard')

@section('content')
<div class="white-box">
    <div class="col-mod-12">
        <div class="col-mod-6 col-lg-6">
                <h3 class="box-title text-success m-b-0">Doa </h3>
                <p class="text-muted m-b-30">Create New Doa</p>
        </div>        
        <div class="col-mod-6 col-lg-6 ">
            <a href="{{ route('show-doa-details') }}" class="waves-effect pull-right"><button class="btn btn-xs btn-info "><i class="fa fa-arrow-circle-left"></i> ALL DETAILS LIST</button></a>
        </div>    
    </div>  
    <div class="clear"></div><hr/>
    <div class="panel-body">

        <form action="{{ route('post.doa-details') }}" method="post">
            {{ csrf_field() }}
            <div class="form-body">
                <h3 class="box-title">Provide Details Doa information</h3>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Doa Title:<span class="text-danger m-1-5">*</span></label>
                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="doa_id" required="">
                                <option value="">Select Product Unit</option>
                                @foreach($doas as $data)
                                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Fojilot: <span class="text-danger m-l-5">*</span></label>
                            <textarea class="form-control" name="fojilot" placeholder="Enter Fojilot" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Arabic Doa: <span class="text-danger m-l-5">*</span></label>
                            <textarea class="form-control" name="doa" placeholder="Enter Arabic Doa" required></textarea>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Meaning: <span class="text-danger m-l-5">*</span></label>
                            <textarea class="form-control" name="meaning" placeholder="Enter Meaning" required></textarea>
                        </div>
                    </div>
                                      
                </div>       
        

            <div class="form-actions">
                <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> SAVE DOA DETAILS INFORMATION</button>
            </div>
        </div>
        </div>
    </form>
    </div>
</div>    
</div>
@endsection