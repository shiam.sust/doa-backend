@extends('admin.dashboard')

@section('content')
<div class="white-box">
    <div class="col-mod-12">
        <div class="col-mod-6 col-lg-6">
                <h3 class="box-title text-success m-b-0">Doa </h3>
                <p class="text-muted m-b-30">Create New Doa</p>
        </div>        
        <div class="col-mod-6 col-lg-6 ">
            <a href="{{ route('show-doa-title') }}" class="waves-effect pull-right"><button class="btn btn-xs btn-info "><i class="fa fa-arrow-circle-left"></i> ALL DOA LIST</button></a>
        </div>    
    </div>  
    <div class="clear"></div><hr/>
    <div class="panel-body">

        <form action="{{ route('update-doa',['id' => $doa->id ]) }}" method="post">
            {{ csrf_field() }}
            <div class="form-body">
                <h3 class="box-title">Provide Doa information</h3>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Doa Title<span class="text-danger m-1-5">*</span></label>
                            <input type="text" id="firstName" class="form-control" name="name" value="{{ $doa->name }}" required=""> 
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Sura Name</label>
                            <input type="text" id="firstName" class="form-control" placeholder="enter sura name" value="{{ $doa->sura }}" name="sura">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Ayat No.</label>
                            <input type="text" id="firstName" class="form-control" placeholder="enter ayat number" value="{{ $doa->ayat_no }}" name="ayat_no">
                        </div>
                    </div>
                                      
                </div>       
        

            <div class="form-actions">
                <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> UPDATE DOA INFORMATION</button>
            </div>
        </div>
        </div>
    </form>
    </div>
</div>    
</div>
@endsection